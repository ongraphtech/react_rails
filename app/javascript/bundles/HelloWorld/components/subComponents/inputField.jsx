import React from 'react';
import PropTypes from 'prop-types';

const Input =(props)=>{
  const {
     type, className, placeholder,id,name,value,
   } = props;
  return(
     <input
      type={type}
      className={className}
      id={id}
      value ={value}
      name={name}
      placeholder={placeholder} />
  )
}

Input.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  id: PropTypes.string,
  name:PropTypes.string,
  value : PropTypes.string,


};

export default Input;
